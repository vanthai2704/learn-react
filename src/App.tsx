import "./styles/index.less"
import { BrowserRouter } from "react-router-dom"
import Loading from "./components/ui-components/Loading"
import { Provider } from "react-redux"
import { store } from "stores"
import { Suspense } from "react"
import Routers from "./routers"

function App() {
  return (
    <>
      <Provider store={store}>
        <BrowserRouter>
          <Suspense fallback={<Loading isForceLoading={true} />}>
            <Routers />
            <Loading />
          </Suspense>
        </BrowserRouter>
      </Provider>
    </>
  )
}

export default App
