export interface ArrayResponse<T> {
  data: T[]
  meta: Meta
}

export interface Response<T> {
  data: T
  meta: {}
}

export interface Meta {
  pagination: {
    page: number
    pageSize: number
    pageCount: number
    total: number
  }
}
