export interface CommonAttribute {
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
}
