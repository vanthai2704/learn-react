import { CommonAttribute } from "./common/CommonAttribute"

export interface Category extends CommonAttribute {
  id: number
  name: string
}
