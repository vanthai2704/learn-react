import { CommonAttribute } from "./common/CommonAttribute";

export interface Post extends CommonAttribute {
  id: number;
  title: string;
  body: string;
  category: string;
  description: string;
  thumbnail: { url: string };
}
