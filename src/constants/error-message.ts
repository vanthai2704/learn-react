export const ERROR_MESSAGES = {
  ERR_SYS: "Lỗi hệ thống",
  API_401: "Phiên đăng nhập đã hết hạn, vui lòng đăng nhập lại",
  API_403: "Bạn không có quyền truy cập chức năng này",
}
