import About from "pages/About"
import Posts from "pages/Post"
import { lazy } from "react"
import { Navigate, useRoutes } from "react-router-dom"
import { useAuthenState } from "stores/authen.slice"
import MainLayout from "../components/layouts/main-layout"
import ProtectedRoute from "./ProtectedRoute"

// lazyload
const Dashboard = lazy(() => import("../pages/Dashboard"))
const Login = lazy(() => import("../pages/Login"))
const NotFound = lazy(() => import("../pages/NotFound"))
const PostDetails = lazy(() => import("../pages/Post/PostDetail"))
const Management = lazy(() => import("../pages/Management"))

const Routers = () => {
  const { user } = useAuthenState()
  const isAuthen = !!user && user.role === "Authenticated"

  const routers = [
    { path: "/", element: <Navigate to="/dashboard" /> },
    {
      path: "/",
      element: <MainLayout />,
      children: [
        {
          path: "dashboard",
          element: <Dashboard />,
        },
        {
          path: "posts/:id",
          element: <PostDetails />,
        },
        {
          path: "posts",
          element: <Posts />,
        },
        {
          path: "about",
          element: <About />,
        },
        {
          path: "not-found",
          element: <NotFound />,
        },
        {
          path: "management",
          element: (
            <ProtectedRoute component={<Management />} redirectPath="/login" isAllowed={isAuthen} />
          ),
        },
      ],
    },
    {
      path: "login",
      element: <Login />,
    },
    {
      path: "*",
      element: <Navigate to="/not-found" />,
    },
  ]

  return useRoutes(routers)
}

export default Routers
