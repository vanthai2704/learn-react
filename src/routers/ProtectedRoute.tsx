import { Navigate } from "react-router-dom"
import { ReactElement } from "react"

export interface ProtectedRouteProps {
  component: ReactElement
  redirectPath: string
  isAllowed: boolean
}

const ProtectedRoute = ({ isAllowed, redirectPath = "/login", component }: ProtectedRouteProps) => {
  if (!isAllowed) {
    return <Navigate to={redirectPath} />
  }

  return component
}

export default ProtectedRoute
