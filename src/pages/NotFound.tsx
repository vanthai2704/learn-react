import { Empty } from "antd"

export default function NotFound() {
  return (
    <div style={{ height: "calc(100vh - 600px)" }}>
      <Empty />
    </div>
  )
}
