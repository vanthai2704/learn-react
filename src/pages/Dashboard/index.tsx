import { Avatar, Col, Comment, Row, Tag } from "antd"
import Search from "antd/lib/input/Search"
import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import PostCard from "components/ui-components/PostCard"
import { Post } from "models/Post"
import "./dashboard.less"
import { http } from "services/api.service"
import { ArrayResponse } from "models/common/BaseResponse"
import { Category } from "models/Category"
import { randomWithList } from "helpers/color.helper"
import Title from "components/ui-components/Title"
import PostCardHorizontal from "components/ui-components/PostCardHorizontal"

function Dashboard(props: any) {
  const navigate = useNavigate()
  const [data, setData] = useState<Post[]>([])
  const [categories, setCategories] = useState<Category[]>([])

  useEffect(() => {
    const callApi = async () => {
      const postPromise = http.get<ArrayResponse<Post>>("api/posts?populate=thumbnail")
      const categoriesPromise = http.get<ArrayResponse<Category>>("api/categories")
      const res = await Promise.all([postPromise, categoriesPromise])
      setData(res[0].data)
      setCategories(res[1].data.slice(0, 6))
    }
    callApi()
  }, [])

  return (
    <div className="dashboard-wrap">
      {/* banner */}
      <div className="bg-gray-100 py-16">
        <div className="container mx-auto flex items-center">
          <div className="w-full">
            <h2 className="font-black text-2xl md:text-5xl">
              Xin chào, Tôi là <span className="text-primary">Eric</span>
            </h2>
            <h2 className="font-black text-xl md:text-3xl">Chào mừng đến với Blog của tôi</h2>
            <p className="text-xl md:text-3xl bold text-gray-600">
              Đừng bỏ lỡ những bài viết mới nhất về công nghệ nhé !
            </p>

            <div className="w-full md:w-3/4">
              <Search placeholder="Nhập từ khóa" allowClear enterButton="Tìm kiếm" size="large" />
            </div>
          </div>
          <div className="banner"></div>
        </div>
      </div>

      {/* content */}
      <div className="mt-10 container mx-auto">
        {/* title */}
        <div className="flex flex-col md:flex-row justify-between">
          <h2 className="text-base mb-4 md:mb-10">FEATURED POSTS</h2>

          <div className="mb-4">
            <span> Hot tags: </span>
            {categories.map((item) => {
              return (
                <Tag color={randomWithList()} key={item.id}>
                  {item.name}
                </Tag>
              )
            })}
          </div>
        </div>

        <Row gutter={[24, 16]}>
          {data?.map((item) => (
            <Col span={24} md={8} key={item.id}>
              <div onClick={() => navigate(`/posts/${item.id}`)}>
                <PostCard post={item} />
              </div>
            </Col>
          ))}
        </Row>

        <Row gutter={[24, 16]}>
          <Col span={24} md={16}>
            <Title title="Related Post"></Title>
            {data.map((post) => {
              return <PostCardHorizontal post={post} key={post.id} />
            })}
          </Col>
          <Col span={24} md={8}>
            <Title title="Last Comment"></Title>

            <Comment
              actions={[<span key="comment-nested-reply-to">Reply to</span>]}
              author={<a>Han Solo</a>}
              avatar={<Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo" />}
              content={
                <p>
                  We supply a series of design principles, practical patterns and high quality
                  design resources (Sketch and Axure).
                </p>
              }
            />

            <Comment
              actions={[<span key="comment-nested-reply-to">Reply to</span>]}
              author={<a>Han Solo</a>}
              avatar={<Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo" />}
              content={
                <p>
                  We supply a series of design principles, practical patterns and high quality
                  design resources (Sketch and Axure).
                </p>
              }
            />

            <Comment
              actions={[<span key="comment-nested-reply-to">Reply to</span>]}
              author={<a>Han Solo</a>}
              avatar={<Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo" />}
              content={
                <p>
                  We supply a series of design principles, practical patterns and high quality
                  design resources (Sketch and Axure).
                </p>
              }
            />
          </Col>
        </Row>
      </div>
    </div>
  )
}

export default Dashboard
