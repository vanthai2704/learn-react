import { LockOutlined, UserOutlined } from "@ant-design/icons"
import { Button, Checkbox, Form, Input } from "antd"
import { LoginForm } from "models/LoginForm"
import React from "react"
import { useNavigate } from "react-router-dom"
import { useAppDispatch } from "stores"
import { login } from "../../stores/authen.slice"
import "./_login.less"

const Login = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const onFinish = async (values: LoginForm) => {
    console.log("Received values of form: ", values)
    // Gửi một POST request
    const data = await dispatch(login(values))
    if (data.type === "login/fulfilled") {
      navigate("/dashboard")
    }
  }

  return (
    <div className="login-page">
      <div className="container mx-auto h-screen flex items-center justify-center">
        <div className="shadow-lg rounded-lg border w-[450px] bg-white p-4">
          <div className="text-center">
            <p className="text-2xl"> Login </p>
          </div>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{ remember: true }}
            onFinish={onFinish}
          >
            <Form.Item
              name="username"
              rules={[{ required: true, message: "Please input your Username!" }]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[{ required: true, message: "Please input your Password!" }]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item>

              <a className="login-form-forgot" href="#$">
                Forgot password
              </a>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Log in
              </Button>
              Or <a href="#$">register now!</a>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  )
}

export default Login
