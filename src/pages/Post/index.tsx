import { Divider, Pagination } from "antd"
import AboutMe from "components/ui-components/AboutMe"
import PostCardHorizontal from "components/ui-components/PostCardHorizontal"
import Title from "components/ui-components/Title"
import { ArrayResponse } from "models/common/BaseResponse"
import { Post } from "models/Post"
import { useState, useEffect } from "react"
import { http } from "services/api.service"

const Posts = () => {
  const [posts, setPosts] = useState<Post[]>([])

  useEffect(() => {
    const callApi = async () => {
      const posts = await http.get<ArrayResponse<Post>>("api/posts?populate=thumbnail")
      setPosts(posts.data)
    }
    callApi()
  }, [])

  return (
    <div className="container mx-auto post-wrap">
      {/* title */}
      <div className="my-12">
        <h1 className="text-4xl font-extrabold">Bài viết</h1>
        <Divider></Divider>
      </div>

      <div className="flex gap-10 mt-5">
        <div className="w-2/3 flex flex-col gap-8">
          {posts.map((post) => {
            return <PostCardHorizontal post={post} key={post.id} />
          })}

          <Pagination defaultCurrent={1} total={50} />
        </div>

        <div className="w-1/3">
          <AboutMe />
        </div>
      </div>
    </div>
  )
}

export default Posts
