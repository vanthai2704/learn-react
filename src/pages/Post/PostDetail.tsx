import React, { useEffect, useState } from "react"
import ReactMarkdown from "react-markdown"
import { useParams } from "react-router-dom"
import remarkGfm from "remark-gfm"
import { Post } from "models/Post"
import rehypeRaw from "rehype-raw"
import { http } from "services/api.service"
import { Response } from "models/common/BaseResponse"

const PostDetails = () => {
  const { id } = useParams()

  const [data, setData] = useState<Post>({ body: "" } as Post)

  useEffect(() => {
    console.log("PostDetails")
    const callApi = async () => {
      const res = await http.get<Response<Post>>("api/posts/" + id)
      setData(res.data)
    }

    callApi()
  }, [])

  return (
    <div className="container mx-auto mt-10">
      <ReactMarkdown
        className="markdown-body"
        children={data?.body}
        remarkPlugins={[remarkGfm]}
        rehypePlugins={[rehypeRaw]}
      />
    </div>
  )
}

export default PostDetails
