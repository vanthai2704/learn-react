import { Avatar, Divider } from "antd"
import { useEffect, useState } from "react"
import "./about.less"

const About = (props: any) => {
  useEffect(() => {}, [])

  return (
    <div className="about-wrap container mx-auto">
      <div className="my-12">
        <h1 className="text-4xl font-extrabold">Giới thiệu</h1>
        <Divider></Divider>
      </div>
      {/* content about */}
      <div className="flex gap-5 items-center">
        <img alt="" src="imgs/ads/ads-1.jpg" className="h-64" />
        <div>
          <h1 className="text-2xl mb-4">Xin chào, Tôi là Eric Nguyễn</h1>
          <p className="my-4">Tôi là một lập trình viên Front-end với 4 năm kinh nghiệm làm Web.</p>
          <p>
            Tôi tạo ra blog này với mục đích lưu trữ và chia sẻ những kiến thức mình đã từng tìm
            hiểu. Tôi mong rằng những bài viết sẽ giúp ích được gì đó cho bạn.
          </p>
          <p>Nếu bạn muốn liên lạc với tôi thì hãy gửi email theo địa chỉ bên dưới.</p>
          Contact for work: <span className="text-lg font-bold mx-4">vanthai116@gmail.com</span>
        </div>
      </div>
    </div>
  )
}

export default About
