import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import { LOCAL_ITEMS } from "constants/local-store"
import { cloneDeep } from "lodash"
import { LoginForm } from "models/LoginForm"
import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { http } from "services/api.service"
import { RootState, useAppDispatch } from "stores"

export interface User {
  id: number
  username: string
  email: string
  provider: string
  confirmed: boolean
  blocked: boolean
  role: "Authenticated" | "Public"
}

export interface IAuthenState {
  jwt: string
  user: User
}

const initialState: IAuthenState = {
  jwt: localStorage.getItem(LOCAL_ITEMS.TOKEN) ?? "",
  user: JSON.parse(localStorage.getItem(LOCAL_ITEMS.USER_INFO) ?? "{}"),
}

const authenSlice = createSlice({
  name: "authen",
  initialState,
  reducers: {
    logout: (state) => {
      localStorage.removeItem(LOCAL_ITEMS.TOKEN)
      localStorage.removeItem(LOCAL_ITEMS.USER_INFO)
      state.jwt = ""
      state.user = {} as User
    },
  },
  extraReducers: (builder) => {
    builder.addCase(login.fulfilled, (state, action) => {
      state.jwt = action.payload.jwt
      state.user = action.payload.user
      localStorage.setItem(LOCAL_ITEMS.TOKEN, state.jwt)
      localStorage.setItem(LOCAL_ITEMS.USER_INFO, JSON.stringify(state.user))
    })
  },
})

const authenReducer = authenSlice.reducer
const authenActions = authenSlice.actions

// selector

const useAuthenState = () => {
  return useSelector((state: RootState) => state.authen)
}

// redux thunk
export const login = createAsyncThunk("login", async ({ username, password }: LoginForm) => {
  const authenInfo = await http.post<IAuthenState>("api/auth/local", {
    identifier: username,
    password: password,
  })
  const userInfoWithRole = await http.get("api/users/me?populate[role][fields][0]=name", {
    headers: {
      Authorization: `Bearer ${authenInfo.jwt}`,
    },
  })

  authenInfo.user = { ...authenInfo.user, role: userInfoWithRole.role.name }
  return authenInfo
})

export { authenReducer, authenActions, useAuthenState }
