import { configureStore } from "@reduxjs/toolkit"
import { ILoadingState, loadingReducer } from "stores/loading.slice"
import { dashboardReducer, IDashboardState } from "./dashboard.slice"
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux"
import { authenReducer, IAuthenState } from "stores/authen.slice"

export interface RootState {
  authen: IAuthenState
  loading: ILoadingState
  dashboard: IDashboardState
}

const store = configureStore({
  reducer: {
    loading: loadingReducer,
    dashboard: dashboardReducer,
    authen: authenReducer,
  },
})

type AppDispatch = typeof store.dispatch
const useAppDispatch: () => AppDispatch = useDispatch
const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export { store, useAppSelector, useAppDispatch }
