import { createSlice } from "@reduxjs/toolkit"
import { useSelector } from "react-redux"
import { Post } from "models/Post"
import { RootState } from "stores"

export interface IDashboardState {
  postList: Post[]
}

const state: IDashboardState = {
  postList: [],
}

const dashboardSlice = createSlice({
  name: "dashboard",
  initialState: state,
  reducers: {
    getPosts: (state, actions) => {
      state.postList = actions.payload
    },
  },
})

const dashboardReducer = dashboardSlice.reducer
const dashboardAction = dashboardSlice.actions

const useDashboardState = () => {
  const dashboardState = useSelector((state: RootState) => state.dashboard.postList)
  return dashboardState
}

export { dashboardReducer, dashboardAction, useDashboardState }
