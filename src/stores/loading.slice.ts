import { createSlice } from "@reduxjs/toolkit"
import { useSelector } from "react-redux"
import { RootState } from "stores"

export interface ILoadingState {
  count: number
}

const initialState: ILoadingState = {
  count: 0,
}

const counterSlice = createSlice({
  name: "loading",
  initialState: initialState,
  reducers: {
    increase: (state) => {
      state.count += 1
    },
    decrease: (state) => {
      if (state.count > 0) {
        state.count -= 1
      }
    },
  },
})

const loadingReducer = counterSlice.reducer
const loadingActions = counterSlice.actions

// selector

const useLoadingState = () => {
  const loadingState = useSelector((state: RootState) => state.loading)
  return loadingState
}

export { loadingReducer, loadingActions, useLoadingState }
