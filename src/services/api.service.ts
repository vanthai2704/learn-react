import { notification } from "antd"
import axios from "axios"
import { loadingActions } from "stores/loading.slice"
import { ERROR_MESSAGES } from "constants/error-message"
import { get } from "lodash"
import { store } from "stores"
import { authenActions } from "stores/authen.slice"

const http = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
  timeout: Number(process.env.REACT_APP_API_TIMEOUT),
  headers: { "Content-Type": "application/json" },
})

// Add a request interceptor
http.interceptors.request.use(
  function (config) {
    let newConfig = { ...config }
    console.log("start")

    store.dispatch(loadingActions.increase())
    if (store.getState().authen.jwt) {
      newConfig.headers = {
        ...config.headers,
        Authorization: `Bearer ${store.getState().authen.jwt}`,
      }
    }

    return newConfig
  },
  function (error) {
    store.dispatch(loadingActions.decrease())
    console.error(error)
    notification["error"]({
      message: "",
      description: ERROR_MESSAGES.ERR_SYS,
      duration: 3,
    })

    return Promise.reject(error)
  }
)

// Add a response interceptor
http.interceptors.response.use(
  function (response) {
    console.log("end")

    store.dispatch(loadingActions.decrease())
    return response.data
  },
  function (error) {
    store.dispatch(loadingActions.decrease())
    const httpErrorCode = get(error, "response.status")
    switch (httpErrorCode) {
      case 403:
        notification["warning"]({
          message: "",
          description: ERROR_MESSAGES.API_403,
          duration: 3,
        })
        return
      case 401:
        store.dispatch(authenActions.logout())
        // window.location.href = window.location.origin + "/login"
        notification["warning"]({
          message: "",
          description: ERROR_MESSAGES.API_401,
          duration: 3,
        })
        return
      default:
        notification["error"]({
          message: "",
          description: ERROR_MESSAGES.ERR_SYS,
          duration: 3,
        })
        return Promise.reject(error)
    }
  }
)

export { http }
