import { useEffect, useState } from "react"
import { Affix, Button, Dropdown, Grid, MenuProps, Space } from "antd"
import { Menu } from "antd"
import { DownOutlined, ProfileOutlined, SearchOutlined, MenuOutlined } from "@ant-design/icons"
import MegaMenu from "../MegaMenu"
import Sidebar from "../Sidebar"
import { Link, useNavigate } from "react-router-dom"
import "./_header.less"
import { useAppDispatch } from "stores"
import { authenActions, useAuthenState } from "stores/authen.slice"
const { useBreakpoint } = Grid

function MainHeader(props: any) {
  const itemsStickyMenu: MenuProps["items"] = [
    {
      label: <Link to="/dashboard">Trang chủ</Link>,
      key: "dashboard",
      icon: <i className="elegant-icon icon_house_alt" />,
    },
    {
      label: "Chủ đề",
      key: "topic",
      icon: <i className="elegant-icon icon_documents_alt" />,
      children: [
        {
          type: "group",
          label: <MegaMenu />,
        },
      ],
    },
    {
      label: <Link to="/posts">Bài viết</Link>,
      key: "posts",
      icon: <i className="elegant-icon icon_pencil-edit"></i>,
    },
    // {
    //   label: "Navigation Three - Submenu",
    //   key: "SubMenu",
    //   icon: <SettingOutlined />,
    //   children: [
    //     {
    //       type: "group",
    //       label: "Item 1",
    //       children: [
    //         {
    //           label: "Option 1",
    //           key: "setting:1",
    //         },
    //         {
    //           label: "Option 2",
    //           key: "setting:2",
    //         },
    //       ],
    //     },
    //     {
    //       type: "group",
    //       label: "Item 2",
    //       children: [
    //         {
    //           label: "Option 3",
    //           key: "setting:3",
    //         },
    //         {
    //           label: "Option 4",
    //           key: "setting:4",
    //         },
    //       ],
    //     },
    //   ],
    // },
    {
      label: <Link to="about">Giới thiệu</Link>,
      icon: <i className="elegant-icon icon_info_alt"></i>,
      key: "about",
    },
  ]

  const screen = useBreakpoint()

  const [isOpenSidebar, setIsOpenSidebar] = useState(false)
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const { user } = useAuthenState()

  const showDefaultDrawer = () => {
    setIsOpenSidebar(true)
  }

  const onClose = () => {
    setIsOpenSidebar(false)
  }

  let loginLogoutButton = <button></button>

  const logout = () => {
    dispatch(authenActions.logout())
    navigate("/login")
  }

  if (!!user && user.role) {
    loginLogoutButton = (
      <Button type="primary" onClick={logout}>
        Logout
      </Button>
    )
  } else {
    loginLogoutButton = (
      <Button
        type="primary"
        onClick={() => {
          navigate("/login")
        }}
      >
        Login
      </Button>
    )
  }

  return (
    <div className="main-header">
      <div className="flex justify-between items-center h-20 content-center center mx-auto container">
        <div>
          <Link to="/" className="inline">
            <img src="/imgs/theme/logo.png" alt="" />
          </Link>
        </div>
        <div className="w-2/3 flex justify-end items-center">
          <div className="hidden md:block">
            <a href="local">
              Layouts
              <DownOutlined style={{ fontSize: "10px" }} />
            </a>

            <Button icon={<ProfileOutlined />} type="link">
              Document
            </Button>

            <span className="h-5 bg-gray-500" style={{ width: "1px" }}></span>

            <Button icon={<SearchOutlined />} type="link">
              Search
            </Button>
          </div>
          {loginLogoutButton}
        </div>
      </div>

      <hr />

      {/* menubar */}
      <Affix>
        <div className="bg-white w-full shadow-lg menu-bar">
          <div className="flex justify-between mx-auto container h-[50px]">
            <div className="w-5/6 flex items-center md:block">
              {screen.xs ? (
                <Dropdown overlay={<Menu mode="vertical" items={itemsStickyMenu} />}>
                  <div className="text-base">
                    <MenuOutlined /> <span>Main Menu</span>
                  </div>
                </Dropdown>
              ) : (
                <Menu mode="horizontal" items={itemsStickyMenu} />
              )}
            </div>

            <div className="text-xl flex gap-2 items-center cus-menu-icon">
              <a className="fb" href="http://localhost:3000/dashboard" title="Share on Facebook">
                <i className="elegant-icon social_facebook"></i>
              </a>
              <a className="tw" href="http://localhost:3000/dashboard" title="Tweet now">
                <i className="elegant-icon social_twitter"></i>
              </a>
              <a className="pt" href="http://localhost:3000/dashboard" title="Pin it">
                <i className="elegant-icon social_pinterest"></i>
              </a>

              <MenuOutlined onClick={showDefaultDrawer} />

              <Sidebar open={isOpenSidebar} size="default" onClose={onClose} />
            </div>
          </div>
        </div>
      </Affix>
    </div>
  )
}

export default MainHeader
