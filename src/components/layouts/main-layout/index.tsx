import React, { PropsWithChildren } from "react"
import { Outlet } from "react-router-dom"
import { Layout } from "antd"
import MainHeader from "./Header"
import MainFooter from "./Footer"
const { Content } = Layout

function MainLayout({ children }: PropsWithChildren) {
  let content = children ? children : <Outlet />

  return (
    <div>
      <Layout className="layout">
        <MainHeader />
        <Content>{content}</Content>
        <MainFooter />
      </Layout>
    </div>
  )
}

export default MainLayout
