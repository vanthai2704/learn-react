import React from "react"
import { Menu } from "antd"
import { ContainerOutlined, DesktopOutlined, PieChartOutlined } from "@ant-design/icons"
import "./mega-menu.less"

const MegaMenu = (props: any) => {
  const item = [
    {
      label: "Option 1",
      key: "1",
      icon: <PieChartOutlined />,
    },
    {
      label: "Option 2",
      key: "2",
      icon: <DesktopOutlined />,
    },
    {
      label: "Option 3",
      key: "3",
      icon: <ContainerOutlined />,
    },
  ]

  return (
    <div className="mega-menu">
      <div className="grid grid-cols-3 gap-8">
        <div>
          <h1>Column1</h1>
          <Menu mode="vertical" items={item} />
        </div>
        <div>
          <h1>Column2</h1>
          <Menu mode="vertical" items={item} />
        </div>
        <div>
          <h1>Column3</h1>
          <Menu mode="vertical" items={item} />
        </div>
      </div>
      {/* <div className="grid grid-cols-2 gap-5 mt-8">
        <img src="/imgs/news/news-1.jpg" alt="mega1" className="max-w-md" />
        <img src="/imgs/news/news-2.jpg" alt="mega2" className="max-w-md" />
      </div> */}
    </div>
  )
}

export default MegaMenu
