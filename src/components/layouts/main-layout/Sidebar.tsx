import { Button, Drawer, Space } from "antd";

export interface SidebarProps {
  size: "default" | "large";
  open: boolean;
  onClose: () => void;
}

export default function Sidebar(props: SidebarProps) {
  const { size, open, onClose } = props;

  return (
    <>
      <Drawer
        title={`${size} Drawer`}
        placement="right"
        size={size}
        onClose={onClose}
        open={open}
        extra={
          <Space>
            <Button onClick={onClose}>Cancel</Button>
            <Button type="primary" onClick={onClose}>
              OK
            </Button>
          </Space>
        }
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Drawer>
    </>
  );
}
