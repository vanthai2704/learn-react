import { User } from "stores/authen.slice"
import "./comment.less"

export interface ICommentProps {
  content: string
  user: User
}

const Comment = (props: ICommentProps) => {
  return <div className="comment-wrap my-6 wow fadeInUp"></div>
}

export default Comment
