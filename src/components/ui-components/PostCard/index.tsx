import * as React from "react"
import { Post } from "models/Post"
import "./post-card.less"

export interface IPostCardProps {
  post: Post
}

const PostCard = ({ post }: IPostCardProps) => {
  const thumbnail = post?.thumbnail?.url

  return (
    <div className="post-card">
      <article className="mb-7 wow fadeInUp animated" data-wow-delay="0.2s">
        <div className="post-card-1 rounded-lg hover-up cursor-pointer">
          <div
            className="post-thumb thumb-overlay img-hover-slide relative"
            style={{
              backgroundImage: `url(${thumbnail})`,
            }}
          >
            <span className="top-right-icon bg-green-500">
              <i className="elegant-icon icon_camera_alt"></i>
            </span>
            <ul className="social-share">
              <li>
                <a href="http://localhost:3000/dashboard">
                  <i className="elegant-icon social_share"></i>
                </a>
              </li>
              <li>
                <a className="fb" href="http://localhost:3000/dashboard" title="Share on Facebook">
                  <i className="elegant-icon social_facebook"></i>
                </a>
              </li>
              <li>
                <a className="tw" href="http://localhost:3000/dashboard" title="Tweet now">
                  <i className="elegant-icon social_twitter"></i>
                </a>
              </li>
              <li>
                <a className="pt" href="http://localhost:3000/dashboard" title="Pin it">
                  <i className="elegant-icon social_pinterest"></i>
                </a>
              </li>
            </ul>
          </div>

          <div className="post-content p-7">
            <div className="entry-meta meta-0 text-base mb-2">
              <a href="category.html.htm">
                <span className="text-info font-bold">{post.category}</span>
              </a>
            </div>
            <div className="d-flex post-card-content">
              <h5 className="post-title text-xl mb-5 font-black">
                <p>{post.title}</p>
              </h5>
              <p className="hidden-description">{post.description}</p>
              <div className="entry-meta meta-1 float-left font-x-small text-uppercase">
                <span className="post-on">27 August</span>
                <span className="time-reading has-dot">12 mins read</span>
                <span className="post-by has-dot">23k views</span>
              </div>
            </div>
          </div>
        </div>
      </article>
    </div>
  )
}

export default PostCard
