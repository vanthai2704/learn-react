import "./title.less"

export interface ITitleProps {
  title: string
}

const Title = ({ title }: ITitleProps) => {
  return (
    <div className="title-wrap my-6 wow fadeInUp">
      <h5>{title}</h5>
    </div>
  )
}

export default Title
