import { Post } from "models/Post"
import * as React from "react"
import { Link } from "react-router-dom"
import "./post-card-horizontal.less"

export interface IPostCardProps {
  post: Post
}

const PostCardHorizontal = ({ post }: IPostCardProps) => {
  const thumbnail = post?.thumbnail?.url

  return (
    <div className="w-full flex flex-col md:flex-row gap-4 items-center mt-7 wow fadeInUp post-card-horizontal">
      <div className="w-full md:w-1/3 overflow-hidden rounded-md">
        <div
          className="post-thumb img-hover-slide relative "
          style={{
            backgroundImage: `url(${thumbnail})`,
          }}
        >
          <ul className="social-share hidden md:block">
            <li>
              <a href="http://localhost:3000/dashboard">
                <i className="elegant-icon social_share"></i>
              </a>
            </li>
            <li>
              <a className="fb" href="http://localhost:3000/dashboard" title="Share on Facebook">
                <i className="elegant-icon social_facebook"></i>
              </a>
            </li>
            <li>
              <a className="tw" href="http://localhost:3000/dashboard" title="Tweet now">
                <i className="elegant-icon social_twitter"></i>
              </a>
            </li>
            <li>
              <a className="pt" href="http://localhost:3000/dashboard" title="Pin it">
                <i className="elegant-icon social_pinterest"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>

      {/* 234 */}
      <div className="items-center w-full md:w-2/3">
        <h3 className="text-info">Fake category</h3>
        <Link to={"/posts/" + post.id}>
          <p className="text-xl font-black hidden-title">{post.title}</p>
        </Link>
        <div className="text-xs font-medium flex gap-3">
          <span>7 AUGUST</span>
          <span>11 MINS READ</span>
          <span>3K VIEWS</span>
        </div>
      </div>
    </div>
  )
}
export default PostCardHorizontal
