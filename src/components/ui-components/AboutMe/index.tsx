import { Avatar, Card } from "antd"
import "./about-me.less"

const AboutMe = () => {
  return (
    <div className="about-me-wrap">
      <Card style={{ width: "100%", borderRadius: "8px" }}>
        <Avatar
          src="imgs/authors/author-2.jpg"
          style={{ width: "60px", height: "60px", border: "1px" }}
        />
        <p className="font-semibold text-xl mt-4">Hello, I'm Thai</p>
        <p className="text-base">
          Hi, I’m Stenven, a Florida native, who left my career in corporate wealth management six
          years ago to embark on a summer of soul searching that would change the course of my life
          forever.
        </p>

        <div className="flex flex-row ">
          <span className="font-bold text-base mr-1">Follow me: </span>

          <div className="social-share flex gap-2">
            <a className="fb" href="http://localhost:3000/dashboard" title="Share on Facebook">
              <i className="elegant-icon social_facebook"></i>
            </a>
            <a className="tw" href="http://localhost:3000/dashboard" title="Tweet now">
              <i className="elegant-icon social_twitter"></i>
            </a>
            <a className="pt" href="http://localhost:3000/dashboard" title="Pin it">
              <i className="elegant-icon social_pinterest"></i>
            </a>
          </div>
        </div>
      </Card>
    </div>
  )
}

export default AboutMe
