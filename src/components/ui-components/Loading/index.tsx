import { Spin } from "antd"
import { memo, useEffect } from "react"
import { useLoadingState } from "../../../stores/loading.slice"
import "./loading.less"

export interface LoadingProps {
  isForceLoading?: boolean
}

const Loading = ({ isForceLoading = false }: LoadingProps) => {
  const loadingState = useLoadingState()
  const isApiRunning = loadingState && loadingState.count > 0

  return (
    <>
      {(isApiRunning || isForceLoading) && (
        <div className="loading-wrap">
          <Spin size="large" />
        </div>
      )}
    </>
  )
}

export default memo(Loading)
