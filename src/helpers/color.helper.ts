import { random } from "lodash"
const colorList = [
  "magenta",
  "red",
  "volcano",
  "orange",
  "gold",
  "lime",
  "green",
  "cyan",
  "blue",
  "geekblue",
  "purple",
  "#f50",
  "#2db7f5",
  "#87d068",
  "#108ee9",
]

export function getRandomColor() {
  var letters = "0123456789ABCDEF"
  var color = "#"
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)]
  }
  return color
}

export function randomWithList() {
  let randomIndex = random(0, colorList.length - 1, false)
  return colorList[randomIndex]
}
